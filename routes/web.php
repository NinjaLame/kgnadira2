<?php
use Illuminate\Support\Facades\Route;

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => ''], function () {

    Route::get('/', 'Portal\HomeController@index')->name('index');
    Route::post('/appointment', 'Portal\HomeController@appointment');
    Route::get('/faq', 'Portal\FaqController@index');
    Route::get('/service', 'Portal\ServiceController@index');
    Route::get('/location', 'Portal\LocationController@index');
    Route::get('/location/{clinic}', 'Portal\LocationController@search');
    Route::get('/about','Portal\AboutController@index');
    Route::get('/schedule', 'Portal\ScheduleController@index');
    Route::get('/schedule/{clinic}', 'Portal\ScheduleController@search');
    Route::get('/promo', 'Portal\PromoController@index');
    Route::get('/gallery', 'Portal\GalleryController@index');

});

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
    Route::get('/', 'BackEnd\DashboardController@index');

    Route::get('/faq', 'BackEnd\FaqController@index')->name('admin.faq.index');
    Route::post('/faq', 'BackEnd\FaqController@store')->name('admin.faq.store');
    Route::get('/faq/{faq}', 'BackEnd\FaqController@show')->name('admin.faq.show');
    Route::put('/faq/{faq}', 'BackEnd\FaqController@update')->name('admin.faq.update');
    Route::delete('/faq/{faq}', 'BackEnd\FaqController@destroy')->name('admin.faq.destroy');

    Route::get('/service', 'BackEnd\ServiceController@index')->name('admin.service.index');
    Route::post('/service', 'BackEnd\ServiceController@store')->name('admin.service.store');
    Route::get('/service/{service}', 'BackEnd\ServiceController@show')->name('admin.service.show');
    Route::put('/service/{service}', 'BackEnd\ServiceController@update')->name('admin.service.update');
    Route::delete('/service/{service}', 'BackEnd\ServiceController@destroy')->name('admin.service.destroy');
    
    Route::get('/promo', 'BackEnd\PromoController@index')->name('admin.promo.index');
    Route::post('/promo', 'BackEnd\PromoController@store')->name('admin.promo.store');
    Route::get('/promo/{promo}', 'BackEnd\PromoController@show')->name('admin.promo.show');
    Route::put('/promo/{promo}', 'BackEnd\PromoController@update')->name('admin.promo.update');
    Route::delete('/promo/{promo}', 'BackEnd\PromoController@destroy')->name('admin.promo.destroy');

    Route::get('/clinic', 'BackEnd\ClinicController@index')->name('admin.clinic.index');
    Route::post('/clinic', 'BackEnd\ClinicController@store')->name('admin.clinic.store');
    Route::get('/clinic/{clinic}', 'BackEnd\ClinicController@show')->name('admin.clinic.show');
    Route::put('/clinic/{clinic}', 'BackEnd\ClinicController@update')->name('admin.clinic.update');
    Route::delete('/clinic/{clinic}', 'BackEnd\ClinicController@destroy')->name('admin.clinic.destroy');
    
    Route::get('/galery', 'BackEnd\GaleryController@index')->name('admin.galery.index');
    Route::post('/galery', 'BackEnd\GaleryController@store')->name('admin.galery.store');
    Route::get('/galery/{galery}', 'BackEnd\GaleryController@show')->name('admin.galery.show');
    Route::put('/galery/{galery}', 'BackEnd\GaleryController@update')->name('admin.galery.update');
    Route::delete('/galery/{galery}', 'BackEnd\GaleryController@destroy')->name('admin.galery.destroy');
    
    Route::get('/schedule', 'BackEnd\ScheduleController@index')->name('admin.schedule.index');
    Route::post('/schedule', 'BackEnd\ScheduleController@store')->name('admin.schedule.store');
    Route::get('/schedule/{schedule}', 'BackEnd\ScheduleController@show')->name('admin.schedule.show');
    Route::put('/schedule/{schedule}', 'BackEnd\ScheduleController@update')->name('admin.schedule.update');
    Route::delete('/schedule/{schedule}', 'BackEnd\ScheduleController@destroy')->name('admin.schedule.destroy');
   
    Route::get('/user', 'BackEnd\UserController@index')->name('admin.user.index');
    Route::post('/user', 'BackEnd\UserController@store')->name('admin.user.store');
    Route::get('/user/{user}', 'BackEnd\UserController@show')->name('admin.user.show');
    Route::put('/user/{user}', 'BackEnd\UserController@update')->name('admin.user.update');
    Route::delete('/user/{user}', 'BackEnd\UserController@destroy')->name('admin.user.destroy');

    Route::get('/patient', 'BackEnd\PatientController@index')->name('admin.patient.index');
    Route::post('/patient', 'BackEnd\PatientController@store')->name('admin.patient.store');
    Route::get('/patient/{patient}', 'BackEnd\PatientController@show')->name('admin.patient.show');
    Route::put('/patient/{patient}', 'BackEnd\PatientController@update')->name('admin.patient.update');
    Route::delete('/patient/{patient}', 'BackEnd\PatientController@destroy')->name('admin.patient.destroy');

    Route::get('/about', 'BackEnd\AboutController@index')->name('admin.about.index');
    Route::post('/about', 'BackEnd\AboutController@store')->name('admin.about.store');


});

Route::group(['prefix' => 'patient', 'middleware' => 'auth'], function () {
    Route::get('/', function ()
    {
        return 'Ahoy';
    });
});


Route::get('/we', 'HomeController@index');