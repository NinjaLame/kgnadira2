@extends('sistem.master')

@section('title', 'Galeri')

@section('page_title_button')

@endsection

@section('page_title')
@include('sistem.layouts.page_title', ['title' =>[
'icon' => 'pe-7s-monitor icon-gradient bg-mean-fruit',
'title' => 'Galery Page',
'submenu' => 'Halaman mengelola data Galeri',
]])
@endsection


@section('content')
<div class="row">
    @include('sistem.layouts.galery_table')
    <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">New Image </h5>
                <form class="" action="/admin/galery" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="position-relative form-group">
                        <label for="title" class="">Title</label>
                        <input name="title" id="title" placeholder="title?" 
                            type="text" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="image" class="">Image</label>
                        <input name="image" id="image" type="file" class="form-control-file">
                    </div>
                    <div class="position-relative form-group">
                        <label for="status" class="">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="hide">Hide</option>
                            <option value="show">Show</option>
                        </select>
                    </div>
                    <button class="mt-1 btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection