@extends('sistem.master')

@section('title', 'FAQ')

@section('page_title_button')

@endsection

@section('page_title')
@include('sistem.layouts.page_title', ['title' =>[
'icon' => 'pe-7s-help1 icon-gradient bg-night-fade',
'title' => 'FAQ Page',
'submenu' => 'Halaman mengelola data FAQ',
]])
@endsection


@section('content')
<div class="row">
    @include('sistem.layouts.faq_table')
    <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">New Faq </h5>
                <form class="" action="/admin/faq" method="POST">
                    @csrf
                    <div class="position-relative form-group">
                        <label for="question" class="">Question</label>
                        <input name="question" id="question" placeholder="What is the question?" 
                            type="text" class="form-control"></div>
                    <div class="position-relative form-group">
                        <label for="answer" class="">Answer</label>
                        <textarea name="answer" id="answer" class="form-control"></textarea>
                    </div>
                    <div class="position-relative form-group">
                        <label for="status" class="">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="hide">Hide</option>
                            <option value="show">Show</option>
                        </select>
                    </div>
                    <button class="mt-1 btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection