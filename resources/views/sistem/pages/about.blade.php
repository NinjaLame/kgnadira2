@extends('sistem.master')

@section('title', 'About')

@section('page_title_button')

@endsection

@section('page_title')
@include('sistem.layouts.page_title', ['title' =>[
'icon' => 'pe-7s-box2 icon-gradient bg-amy-crisp',
'title' => 'About Page',
'submenu' => 'Halaman mengelola data Tentang Kami',
]])
@endsection


@section('content')
<div class="row">
    <div class="col-md-8 offset-md-1">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">About </h5>
                <form class="" action="/admin/about" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$about->id}}">
                    <div class="position-relative form-group">
                        <label for="title" class="">Title</label>
                        <input name="title" id="title" placeholder="" 
                            type="text" class="form-control" value="{{$about->title}}"></div>
                    <div class="position-relative form-group">
                        <label for="text_1" class="">Visi</label>
                        <textarea name="text_1" id="text_1" class="form-control">{{$about->text_1}}</textarea>
                    </div>
                    <div class="position-relative form-group">
                        <label for="text_2" class="">Misi</label>
                        <textarea name="text_2" id="text_2" class="form-control">{{$about->text_1}}</textarea>
                    </div>
                    <div class="position-relative form-group">
                        <label for="desc" class="">Detail</label>
                        <textarea name="desc" id="desc" class="form-control">{{$about->desc}}</textarea>
                    </div>
                    <div class="position-relative form-group">
                        <label for="phone" class="">Phone Number</label>
                        <input name="phone" id="phone" placeholder="Phone Number?" 
                            type="text" class="form-control" value="{{$about->phone}}"></div>
                    <div class="position-relative form-group">
                        <label for="email" class="">Email</label>
                        <input name="email" id="email" placeholder="Email?" 
                            type="text" class="form-control" value="{{$about->email}}">
                    </div>
                    
                    <button class="mt-1 btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection