@extends('sistem.master')

@section('title', 'Klinik')

@section('page_title_button')

@endsection

@section('page_title')
@include('sistem.layouts.page_title', ['title' =>[
'icon' => 'pe-7s-box2 icon-gradient bg-amy-crisp',
'title' => 'Clinic Page',
'submenu' => 'Halaman mengelola data Klinik',
]])
@endsection


@section('content')
<div class="row">
    @include('sistem.layouts.clinic_table')
    <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">New Clinic </h5>
                <form class="" action="/admin/clinic" method="POST">
                    @csrf
                    <div class="position-relative form-group">
                        <label for="name" class="">Name</label>
                        <input name="name" id="name" placeholder="What is the name?" 
                            type="text" class="form-control"></div>
                    <div class="position-relative form-group">
                        <label for="address" class="">Address</label>
                        <textarea name="address" id="address" class="form-control"></textarea>
                    </div>
                    <div class="position-relative form-group">
                        <label for="phone" class="">Phone Number</label>
                        <input name="phone" id="phone" placeholder="What is the phone?" 
                            type="text" class="form-control"></div>
                    <div class="position-relative form-group">
                        <label for="day_open" class="">Day Operate</label>
                        <input name="day_open" id="day_open" placeholder="When this operate?" 
                            type="text" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="opened_at" class="">Time Open</label>
                        <input name="opened_at" id="opened_at" placeholder="When this open?" 
                            type="text" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="closed_at" class="">Time Close</label>
                        <input name="closed_at" id="closed_at" placeholder="When this close?" 
                            type="text" class="form-control">
                    </div>
                    
                    <button class="mt-1 btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection