@extends('sistem.master')

@section('title', 'Perawatan')

@section('page_title_button')
<form action="/admin/service">
    <button type="submit" data-toggle="tooltip" title="Back to service" data-placement="bottom"
        class="btn-shadow mr-3 btn btn-dark">
        Back
    </button>
</form>
@endsection

@section('page_title')
@include('sistem.layouts.page_title', ['title' =>[
'icon' => 'pe-7s-menu icon-gradient bg-ripe-malin',
'title' => 'Service Page',
'submenu' => 'Halaman mengelola data Perawatan',
]])
@endsection


@section('content')
<link rel="stylesheet" href="/portal/css/glyphter.css">

<div class="row">
    @include('sistem.layouts.service_table')
    <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Edit Service </h5>
                <form class="" action="/admin/service/{{$selected->id}}" method="POST">
                    <input name="_method" type="hidden" value="PUT">
                    @csrf
                    <div class="position-relative form-group">
                        <label for="name" class="">Name</label>
                        <input name="name" id="name" placeholder="Service name?" 
                            type="text" class="form-control" value="{{$selected->name}}"></div>
                    <div class="position-relative form-group">
                        <label for="description" class="">Description</label>
                        <textarea name="description" id="description" class="form-control">{{$selected->description}}</textarea>
                    </div>
                    <div class="position-relative form-group">
                        <label for="icon" class="">Icon </label>
                        <select name="icon" id="icon" class="form-control" 
                            onchange="document.getElementById('iconSample').className=this.options[this.selectedIndex].value">
                            @foreach (App\Service::$iconEnum as $item)
                            <option value="{{$item}}" {!! $selected->icon == $item ? 'selected' : ''!!}>{{$item}}</option>
                            @endforeach
                        </select>
                        <i class="{{$selected->icon}}" id="iconSample"></i>
                        
                    </div>
                    <button type="submit" class="mt-1 btn btn-primary">Save Edit</button>
                    <a href="/admin/faq" class="mt-1 btn btn-warning">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection