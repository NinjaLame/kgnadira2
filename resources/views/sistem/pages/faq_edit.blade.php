@extends('sistem.master')

@section('title', 'FAQ')

@section('page_title_button')
<form action="/admin/faq">
    <button type="submit" data-toggle="tooltip" title="Create new data" data-placement="bottom"
        class="btn-shadow mr-3 btn btn-dark">
        Back
    </button>
</form>
@endsection

@section('page_title')
@include('sistem.layouts.page_title', ['title' =>[
'icon' => 'pe-7s-help1 icon-gradient bg-night-fade',
'title' => 'Clinic Page',
'submenu' => 'Halaman mengelola data FAQ',
]])
@endsection


@section('content')
<div class="row">
    @include('sistem.layouts.faq_table')

    <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Edit Faq </h5>
                <form class="" action="/admin/faq/{{$selected->id}}" method="POST">
                    <input name="_method" type="hidden" value="PUT">
                    @csrf
                    <div class="position-relative form-group">
                        <label for="question" class="">Question</label>
                        <input name="question" id="question" placeholder="What is the question?" 
                            type="text" class="form-control" value="{{$selected->question}}"></div>
                    <div class="position-relative form-group">
                        <label for="answer" class="">Answer</label>
                        <textarea name="answer" id="answer" class="form-control">{{$selected->answer}}</textarea>
                    </div>
                    <div class="position-relative form-group">
                        <label for="status" class="">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="hide" {!! $selected->status == 'hide' ? 'selected' : ''!!}>Hide</option>
                            <option value="show" {!! $selected->status == 'show' ? 'selected' : ''!!}>Show</option>
                        </select>
                    </div>
                    <button type="submit" class="mt-1 btn btn-primary">Save Edit</button>
                    <a href="/admin/faq" class="mt-1 btn btn-warning">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection