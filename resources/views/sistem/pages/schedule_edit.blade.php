@extends('sistem.master')

@section('title', 'Jadwal')

@section('page_title_button')
<form action="/admin/schedule">
    <button type="submit" data-toggle="tooltip" title="Back to service" data-placement="bottom"
        class="btn-shadow mr-3 btn btn-dark">
        Back
    </button>
</form>
@endsection

@section('page_title')
@include('sistem.layouts.page_title', ['title' =>[
'icon' => 'pe-7s-monitor icon-gradient bg-mean-fruit',
'title' => 'Schedule Page',
'submenu' => 'Halaman mengelola data Jadwal Praktek',
]])
@endsection


@section('content')
<div class="row">
    @include('sistem.layouts.schedule_table')
    <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Edit Schedule </h5>
                <form class="" action="/admin/schedule/{{$selected->id}}" method="POST">
                    <input name="_method" type="hidden" value="PUT">
                    @csrf
                    <div class="position-relative form-group">
                        <label for="exampleSelect" class="">Doctor</label>
                        <select name="doctor_id" id="doctor_id" class="form-control">
                            @foreach ($doctors as $item)
                            <option value="{{$item->id}}" {!! $selected->doctor_id == $item->id ? 'selected' : ''!!}>{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="position-relative form-group">
                        <label for="clinic_id" class="">Clinic</label>
                        <select name="clinic_id" id="clinic_id" class="form-control">
                            @foreach ($clinics as $item)
                            <option value="{{$item->id}}" {!! $selected->clinic_id == $item->id ? 'selected' : ''!!}>{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="position-relative form-group">
                        <label for="day" class="">Day</label>
                        <select name="day" id="day" class="form-control">
                            <option value="" disabled selected>Select day</option>
                            @foreach (App\Schedule::$dayEnum as $item)
                            <option value="{{$item}}" {!! $selected->day == $item ? 'selected' : ''!!}>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="position-relative form-group">
                        <label for="started_at" class="">Start</label>
                        <input name="started_at" id="started_at" placeholder="hour Start" type="text"
                            value="{{$selected->started_at}}" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="finished_at" class="">End</label>
                        <input name="finished_at" id="finished_at" placeholder="hour  End" type="text"
                            value="{{$selected->finished_at}}" class="form-control">
                    </div>
                    <button type="submit" class="mt-1 btn btn-primary">Save Edit</button>
                    <a href="/admin/schedule" class="mt-1 btn btn-warning">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection