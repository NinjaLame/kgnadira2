@extends('sistem.master')

@section('title', 'Perawatan')

@section('page_title_button')

@endsection

@section('page_title')
@include('sistem.layouts.page_title', ['title' =>[
'icon' => 'pe-7s-help1 icon-gradient bg-night-fade',
'title' => 'Service Page',
'submenu' => 'Halaman mengelola data Perawatan',
]])
@endsection


@section('content')
<link rel="stylesheet" href="/portal/css/glyphter.css">

<div class="row">
    @include('sistem.layouts.service_table')
    <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">New Service </h5>
                <form class="" action="/admin/service" method="POST">
                    @csrf
                    <div class="position-relative form-group">
                        <label for="name" class="">Name</label>
                        <input name="name" id="name" placeholder="Service name?" 
                            type="text" class="form-control"></div>
                    <div class="position-relative form-group">
                        <label for="description" class="">Description</label>
                        <textarea name="description" id="description" class="form-control"></textarea>
                    </div>
                    <div class="position-relative form-group">
                        <label for="icon" class="">Icon </label>
                        <select name="icon" id="icon" class="form-control" 
                            onchange="document.getElementById('iconSample').className=this.options[this.selectedIndex].value">
                            @foreach (App\Service::$iconEnum as $item)
                            <option value="{{$item}}">{{$item}}</option>
                            @endforeach
                        </select>
                        <i class="flaticon-tooth" id="iconSample"></i>
                        
                    </div>
                    <button class="mt-1 btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection