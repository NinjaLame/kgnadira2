@extends('sistem.master')

@section('title', 'User')

@section('page_title_button')

@endsection

@section('page_title')
@include('sistem.layouts.page_title', ['title' =>[
'icon' => 'pe-7s-help1 icon-gradient bg-night-fade',
'title' => 'User Page',
'submenu' => 'Halaman mengelola data User',
]])
@endsection

@section('content')
<div class="row">
    @include('sistem.layouts.user_table')
    <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Edit User </h5>
                <form class="" action="/admin/user/{{$selected->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input name="_method" type="hidden" value="PUT">

                    <div class="position-relative form-group">
                        <label for="name" class="">Name</label>
                        <input name="name" id="name" value="{{$selected->name}}" placeholder="User name?" type="text" class="form-control"/>
                    </div>
                    <div class="position-relative form-group">
                        <label for="password" class="">Password</label>
                        <input name="password" id="password" placeholder="User password?" type="password" class="form-control"/>
                    </div>
                    <div class="position-relative form-group">
                        <label for="email" class="">Email</label>
                        <input name="email" id="email" value="{{$selected->email}}" placeholder="User email?" type="email" class="form-control"/>
                    </div>
                    <div class="position-relative form-group">
                        <label for="address" class="">Address</label>
                        <textarea name="address" id="address" class="form-control">{{$selected->address}}</textarea>
                    </div>
                    <div class="position-relative form-group">
                        <label for="gender" class="">Gender </label>
                        <select name="gender" id="gender" class="form-control">
                            <option value="" selected disabled>Select gender</option>
                            @foreach (App\User::$genderEnum as $item)
                            <option value="{{$item}}" {{$selected->gender == $item ? 'selected' : ''}}>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="position-relative form-group">
                        <label for="role" class="">Role </label>
                        <select name="role" id="role" class="form-control">
                            <option value="" selected disabled>Select role</option>
                            @foreach (App\User::$roleEnum as $item)
                            <option value="{{$item}}" {{$selected->role == $item ? 'selected' : ''}}>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="position-relative form-group">
                        <label for="status" class="">Status </label>
                        <select name="status" id="status" class="form-control">
                            <option value="" selected disabled>Select status</option>
                            @foreach (App\User::$statusEnum as $item)
                            <option value="{{$item}}" {{$selected->status == $item ? 'selected' : ''}}>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="position-relative form-group">
                        <label for="profile_picture" class="">Profile Picture</label>
                        <input name="profile_picture" id="profile_picture" type="file" class="form-control-file">
                    </div>
                    <button class="mt-1 btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection