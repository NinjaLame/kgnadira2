@extends('sistem.master')

@section('title', 'Jadwal')

@section('page_title_button')

@endsection

@section('page_title')
@include('sistem.layouts.page_title', ['title' =>[
'icon' => 'pe-7s-monitor icon-gradient bg-mean-fruit',
'title' => 'Schedule Page',
'submenu' => 'Halaman mengelola data Jadwal Praktek',
]])
@endsection


@section('content')
<div class="row">
    @include('sistem.layouts.schedule_table')
    <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">New Schedule </h5>
                <form class="" action="/admin/schedule" method="POST">
                    @csrf
                    <div class="position-relative form-group">
                        <label for="exampleSelect" class="">Doctor</label>
                        <select name="doctor_id" id="doctor_id" class="form-control">
                            <option value="" disabled selected>Select doctor</option>
                            @foreach ($doctors as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="position-relative form-group">
                        <label for="clinic_id" class="">Clinic</label>
                        <select name="clinic_id" id="clinic_id" class="form-control">
                            <option value="" disabled selected>Select clinic</option>
                            @foreach ($clinics as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="position-relative form-group">
                        <label for="day" class="">Day</label>
                        <select name="day" id="day" class="form-control">
                            <option value="" disabled selected>Select day</option>
                            @foreach (App\Schedule::$dayEnum as $item)
                            <option value="{{$item}}">{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="position-relative form-group">
                        <label for="started_at" class="">Start</label>
                        <input name="started_at" id="started_at" placeholder="hour Start" type="text"
                            class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="finished_at" class="">End</label>
                        <input name="finished_at" id="finished_at" placeholder="hour  End" type="text"
                            class="form-control">
                    </div>
                    <button class="mt-1 btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection