@extends('sistem.master')

@section('title', 'Galeri')

@section('page_title_button')
<form action="/admin/galery">
    <button type="submit" data-toggle="tooltip" title="Create new data" data-placement="bottom"
        class="btn-shadow mr-3 btn btn-dark">
        Back
    </button>
</form>
@endsection

@section('page_title')
@include('sistem.layouts.page_title', ['title' =>[
'icon' => 'pe-7s-monitor icon-gradient bg-mean-fruit',
'title' => 'Galery Page',
'submenu' => 'Halaman mengelola data Galeri',
]])
@endsection


@section('content')
<div class="row">
    @include('sistem.layouts.galery_table')
    <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Edit Image </h5>
                <form class="" action="/admin/galery/{{$selected->id}}" method="POST" enctype="multipart/form-data">
                    <input name="_method" type="hidden" value="PUT">
                    @csrf
                    <div class="position-relative form-group">
                        <label for="title" class="">Title</label>
                        <input name="title" id="title" placeholder="title?" 
                            type="text" class="form-control" value="{{$selected->title}}">
                    </div>
                    <div class="position-relative form-group">
                        <label for="image" class="">Image</label>
                        <input name="image" id="image" type="file" class="form-control-file">
                    </div>
                    <div class="position-relative form-group">
                        <label for="status" class="">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="hide" {!! $selected->status == 'hide' ? 'selected' : ''!!}>Hide</option>
                            <option value="show" {!! $selected->status == 'show' ? 'selected' : ''!!}>Show</option>
                        </select>
                    </div>
                    <button type="submit" class="mt-1 btn btn-primary">Save Edit</button>
                    <a href="/admin/galery" class="mt-1 btn btn-warning">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection