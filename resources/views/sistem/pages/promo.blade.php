@extends('sistem.master')

@section('title', 'Promo')

@section('page_title_button')

@endsection

@section('page_title')
@include('sistem.layouts.page_title', ['title' =>[
'icon' => 'pe-7s-wallet icon-gradient bg-plum-plate',
'title' => 'Promo Page',
'submenu' => 'Halaman mengelola data Promosi',
]])
@endsection


@section('content')
<div class="row">
    @include('sistem.layouts.promo_table')
    <div class="col-md-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">New Promo </h5>
                <form class="" action="/admin/promo" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="position-relative form-group">
                        <label for="title" class="">Title</label>
                        <input name="title" id="title" placeholder="Title?" type="text" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="description" class="">Description</label>
                        <textarea name="description" id="description" class="form-control"></textarea>
                    </div>
                    <div class="position-relative form-group"><label for="image" class="">Image</label><input
                            name="image" id="image" type="file" class="form-control-file">
                    </div>
                    <div class="position-relative form-group">
                        <label for="date_start" class="">Date Start</label>
                        <input name="date_start" id="date_start" placeholder="Promo Start" type="text" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label for="date_end" class="">Date End</label>
                        <input name="date_end" id="date_start" placeholder="Promo End" type="text" class="form-control">
                    </div>
                    <button class="mt-1 btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection