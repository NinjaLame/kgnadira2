<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                    data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                @if (Auth::check())
                @if(Auth::user()->role == "admin")
                    <li class="app-sidebar__heading">Dashboards</li>
                    <li>
                        <a href="/admin" class="">
                            <i class="metismenu-icon pe-7s-rocket"></i>
                            Dashboard Admin
                        </a>
                    </li>
                    

                    <li>
                        <a {!! !in_array(Request::segment(2),['faq','service','promo','clinic','galery','about','schedule'],true)?  '' : 'class="mm-active" aria-expanded=true' !!} href="#">
                            <i class="metismenu-icon pe-7s-world"></i>
                            Web Portal
                            <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                        </a>
                        <ul {!! !in_array(Request::segment(2),['faq','service','promo','clinic','galery','about','schedule'],true) ? '' : 'class="mm-collapse mm-show"' !!}>
                            <li>
                                <a {!!Request::segment(2)=='faq' ? 'class="mm-active"' : '' !!} href="/admin/faq">
                                    <i class="metismenu-icon"></i>
                                    FAQ
                                </a>
                            </li>
                            <li>
                                <a {!!Request::segment(2)=='service' ? 'class="mm-active"' : '' !!} href="/admin/service">
                                    <i class="metismenu-icon">
                                    </i>Perawatan
                                </a>
                            </li>
                            <li>
                                <a {!!Request::segment(2)=='promo' ? 'class="mm-active"' : '' !!} href="/admin/promo">
                                    <i class="metismenu-icon">
                                    </i>Promo
                                </a>
                            </li>
                            <li>
                                <a {!!Request::segment(2)=='clinic' ? 'class="mm-active"' : '' !!} href="/admin/clinic">
                                    <i class="metismenu-icon">
                                    </i>Klinik
                                </a>
                            </li>
                            <li>
                                <a {!!Request::segment(2)=='galery' ? 'class="mm-active"' : '' !!} href="/admin/galery">
                                    <i class="metismenu-icon">
                                    </i>Galeri
                                </a>
                            </li>
                            <li>
                                <a {!!Request::segment(2)=='about' ? 'class="mm-active"' : '' !!} href="/admin/about">
                                    <i class="metismenu-icon">
                                    </i>About
                                </a>
                            </li>
                            <li>
                                <a {!!Request::segment(2)=='schedule' ? 'class="mm-active"' : '' !!} href="/admin/schedule">
                                    <i class="metismenu-icon">
                                    </i>Jadwal Praktek
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="app-sidebar__heading">Data</li>
                    <li>
                        <a href="/admin/user" {!!Request::segment(2)=='user' ? 'class="mm-active"' : '' !!}>
                            <i class="metismenu-icon pe-7s-rocket"></i>
                            Users
                        </a>
                    </li>
                    <li>
                        <a href="/admin/patient" {!!Request::segment(2)=='patient' ? 'class="mm-active"' : '' !!}>
                            <i class="metismenu-icon pe-7s-rocket"></i>
                            Patient
                        </a>
                    </li>
                    @endif
                @endif
            </ul>
        </div>
    </div>
</div>