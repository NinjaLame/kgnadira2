<div class="col-lg-8">
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Daftar FAQ</h5>
            <table class="mb-1 table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Berlaku</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $i = 1;
                    @endphp
                    @foreach ($promotions as $item)
                    @php
                    $description = strlen($item->description)>25 ? Str::limit( $item->description,20).'...' :
                    $item->description;
                    @endphp
                    <tr>
                        <th scope="row">{{$i}}</th>
                        <td>{{$item->title}}</td>
                        <td>{{$description}}</td>
                        <td><a href="{{$item->image}}"><img src="{{$item->image}}" alt="" style="width:30px; height:30px"></a></td>
                        <td>{{$item->date_start.' - '.$item->date_end}}</td>
                        <td class="d-flex">
                            <a href="/admin/promo/{{$item->id}}"
                                class="mt-0 mb-0 border-0 btn-transition btn btn-outline-primary">Edit</a> |
                            <form class="" action="/admin/promo/{{$item->id}}" method="POST">
                                <input name="_method" type="hidden" value="DELETE">
                                @csrf
                                <button type='submit'
                                    class="mt-0 mb-0 border-0 btn-transition btn btn-outline-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @php
                    $i++;
                    @endphp
                    @endforeach
                </tbody>
            </table>
            {{$promotions->links()}}
        </div>
    </div>
</div>