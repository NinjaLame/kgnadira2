<div class="col-lg-8">
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Daftar FAQ</h5>
            <table class="mb-1 table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $i = 1;
                    @endphp
                    @foreach ($galery as $item)
                    
                    <tr>
                        <th scope="row">{{$i}}</th>
                        <td>{{$item->title}}</td>
                        <td><a href="{{$item->image}}"><img src="{{$item->image}}" alt="" style="width:50px; height:50px"></a></td>
                        <td>{{$item->status}}</td>
                        <td class="d-flex">
                            <a href="/admin/galery/{{$item->id}}"
                                class="mt-0 mb-0 border-0 btn-transition btn btn-outline-primary">Edit</a> |
                            <form class="" action="/admin/galery/{{$item->id}}" method="POST">
                                <input name="_method" type="hidden" value="DELETE">
                                @csrf
                                <button type='submit'
                                    class="mt-0 mb-0 border-0 btn-transition btn btn-outline-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @php
                    $i++;
                    @endphp
                    @endforeach
                </tbody>
            </table>
            {{$galery->links()}}
        </div>
    </div>
</div>