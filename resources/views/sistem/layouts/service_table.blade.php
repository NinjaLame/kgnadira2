<div class="col-lg-8">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Daftar FAQ</h5>
                <table class="mb-1 table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Icon</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($services as $item)
                        @php
                        $name = strlen($item->name)>25 ? Str::limit( $item->name,20).'...' : $item->name;
                        $description = strlen($item->description)>25 ? Str::limit( $item->description,20).'...' :
                        $item->description;
                        @endphp
                        <tr>
                            <th scope="row">{{$i}}</th>
                            <td>{{$name}}</td>
                            <td>{{$description}}</td>
                            <td><i class="{{$item->icon}}"></i></td>
                            <td class="d-flex">
                                <a href="/admin/service/{{$item->id}}"
                                    class="mt-0 mb-0 border-0 btn-transition btn btn-outline-primary">Edit</a> |
                                <form class="" action="/admin/service/{{$item->id}}" method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                    @csrf
                                    <button type='submit'
                                        class="mt-0 mb-0 border-0 btn-transition btn btn-outline-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @php
                        $i++;
                        @endphp
                        @endforeach
                    </tbody>
                </table>
                {{$services->links()}}
            </div>
        </div>
    </div>