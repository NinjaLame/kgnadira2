<div class="col-lg-8">
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Daftar FAQ</h5>
            <table class="mb-1 table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Clinic</th>
                        <th>Doctor</th>
                        <th>Day</th>
                        <th>Time</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $i = 1;
                    @endphp
                    @foreach ($schedules as $item)
                    <tr>
                        <th scope="row">{{$i}}</th>
                        <td>{{$item->clinic->name}}</td>
                        <td>{{$item->doctor->user->name}}</td>
                        <td>{{$item->day}}</td>
                        <td>{{$item->started_at.' - '.$item->finished_at}}</td>
                        <td class="d-flex">
                            <a href="/admin/schedule/{{$item->id}}"
                                class="mt-0 mb-0 border-0 btn-transition btn btn-outline-primary">Edit</a> |
                            <form class="" action="/admin/schedule/{{$item->id}}" method="POST">
                                <input name="_method" type="hidden" value="DELETE">
                                @csrf
                                <button type='submit'
                                    class="mt-0 mb-0 border-0 btn-transition btn btn-outline-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @php
                    $i++;
                    @endphp
                    @endforeach
                </tbody>
            </table>
            {{$schedules->links()}}
        </div>
    </div>
</div>