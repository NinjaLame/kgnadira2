<div class="col-lg-8">
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Daftar FAQ</h5>
            <table class="mb-1 table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Question</th>
                        <th>Answer</th>
                        <th>Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $i = 1;
                    @endphp
                    @foreach ($faqs as $item)
                    @php
                    $answer = strlen($item->answer)>25 ? Str::limit( $item->answer,20).'...' : $item->answer;
                    $question = strlen($item->question)>25 ? Str::limit( $item->question,20).'...' :
                    $item->question;
                    @endphp
                    <tr>
                        <th scope="row">{{$i}}</th>
                        <td>{{$question}}</td>
                        <td>{{$answer}}</td>
                        <td>{{$item->status}}</td>
                        <td class="d-flex">
                            <a href="/admin/faq/{{$item->id}}"
                                class="mt-0 mb-0 border-0 btn-transition btn btn-outline-primary">Edit</a> |
                            <form class="" action="/admin/faq/{{$item->id}}" method="POST">
                                <input name="_method" type="hidden" value="DELETE">
                                @csrf
                                <button type='submit'
                                    class="mt-0 mb-0 border-0 btn-transition btn btn-outline-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @php
                    $i++;
                    @endphp
                    @endforeach
                </tbody>
            </table>
            {{$faqs->links()}}
        </div>
    </div>
</div>