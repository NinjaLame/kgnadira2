<div class="col-lg-8">
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Daftar FAQ</h5>
            <div class="table-responsive">
                <table class="mb-1 table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Day Operate</th>
                            <th>Open</th>
                            <th>Close</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($clinics as $item)
                        @php
                        $address = strlen($item->address)>25 ? Str::limit( $item->address,20).'...' :
                        $item->address;
                        @endphp
                        <tr>
                            <th scope="row">{{$i}}</th>
                            <td>{{$item->name}}</td>
                            <td>{{$address}}</td>
                            <td>{{$item->phone}}</td>
                            <td>{{$item->day_open}}</td>
                            <td>{{$item->opened_at}}</td>
                            <td>{{$item->closed_at}}</td>
                            <td class="d-flex">
                                <a href="/admin/clinic/{{$item->id}}"
                                    class="mt-0 mb-0 border-0 btn-transition btn btn-outline-primary">Edit</a> |
                                <form class="" action="/admin/clinic/{{$item->id}}" method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                    @csrf
                                    <button type='submit'
                                        class="mt-0 mb-0 border-0 btn-transition btn btn-outline-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @php
                        $i++;
                        @endphp
                        @endforeach
                    </tbody>
                </table>

            </div>
            {{$clinics->links()}}
        </div>
    </div>
</div>