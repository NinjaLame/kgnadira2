<div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                <i class="{{$title['icon']}}">
                    </i>
                </div>
                <div>{{$title['title']}}
                    <div class="page-title-subheading">{{$title['submenu']}}
                    </div>
                </div>
            </div>
            <div class="page-title-actions">
                @yield('page_title_button')
                
            </div>
        </div>
    </div>