<div class="col-lg-8">
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Daftar FAQ</h5>
            <div class="table-responsive">
                <table class="mb-1 table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Profile</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Gender</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($users as $item)
                        @php
                        $address = strlen($item->address)>25 ? Str::limit( $item->address,20).'...' :
                        $item->address;
                        @endphp
                        <tr>
                            <th scope="row">{{$i}}</th>
                            <td>{{$item->name}}</td>
                            <td><a href="{{$item->profile_picture}}"><img src="{{$item->profile_picture}}" alt="" style="width:50px; height:50px"></a></td>
                            <td>{{$item->email}}</td>
                            <td>{{$address}}</td>
                            <td>{{$item->gender}}</td>
                            <td>{{$item->role}}</td>
                            <td>{{$item->status}}</td>
                            <td class="d-flex">
                                <a href="/admin/user/{{$item->id}}"
                                    class="mt-0 mb-0 border-0 btn-transition btn btn-outline-primary">Edit</a> |
                                <form class="" action="/admin/user/{{$item->id}}" method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                    @csrf
                                    <button type='submit'
                                        class="mt-0 mb-0 border-0 btn-transition btn btn-outline-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @php
                        $i++;
                        @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{$users->links()}}
        </div>
    </div>
</div>