<!--================ appointment Area Starts =================-->
<section class="appointment-area">
    <div class="container">

        <div class="appointment-inner">
            <div class="row">
                <div class="col-sm-12 col-lg-5 offset-lg-1">
                    <h3>Have Some Questions?</h3>
                    <div class="accordion" id="accordionExample">
                        <?php $index = 0; ?>
                        @foreach ($faqs as $faq)
                        <div class="card">
                            <div class="card-header" id="heading{{$faq->id}}">
                                <h5 class="mb-0">
                                    <button class="btn btn-link {{$index==0 ? '' :'collapsed' }}" type="button"
                                        data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="true"
                                        aria-controls="collapse{{$faq->id}}">
                                        {{$faq->question}}
                                    </button>

                                </h5>
                            </div>

                            <div id="collapse{{$faq->id}}" class="collapse {{$index==0 ? 'show' :'' }}"
                                aria-labelledby="heading{{$faq->id}}" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{$faq->answer}}
                                </div>
                            </div>
                        </div>
                        <?php $index++; ?>
                        @endforeach

                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="appointment-form">
                        <h3>Make an Appointment</h3>
                        <form action="/appointment" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="name">{{ __('Full Name') }}</label>
                                <input id="name" type="text" name="name"
                                    class="form-control @error('name') is-invalid @enderror" name="name"
                                    value="{{ old('name') }}" required autocomplete="email">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" name="email"
                                    class="form-control @error('email') is-invalid @enderror" name="email"
                                    value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Message</label>
                                <textarea name="message" cols="20" rows="7" name="message" placeholder="Message"
                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Message'"
                                    required></textarea>
                            </div>
                            <button type="submit" class="main_btn">Daftar</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>


    </div>
</section>
<!--================ appointment Area End =================-->