<!--================About  Area =================-->
<section class="about-area">
    <div class="container" style="background: url('');">
        <div class="row align-items-center">
            <div class="col-md-10 offset-md-1 col-lg-6 offset-lg-6 offset-xl-7 col-xl-5">
                <div class="about-content">
                    <h4>{{$about->title}}<br></h4>
                    <h6>{{$about->text_1}}</h6>
                    <p>{{$about->text_2}}</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================About Area End =================-->