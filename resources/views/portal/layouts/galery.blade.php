<div class="whole-wrap">
    <div class="container">
        <div class="section-top-border">
            <h3 class="title_color">Galeri Klinik Gigi Nadira</h3>
            <div class="row gallery-item">
                @foreach ($images as $image)
                <div class="col-md-4">
                    <a href="{{$image->image}}" class="img-gal">
                        <div class="single-gallery-image" style="background: url({{$image->image}});"></div>
                    </a>
                </div>
                @endforeach
                
                
            </div>
            {{$images->links()}}
        </div>
    </div>
</div>