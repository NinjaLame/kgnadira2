<!--================ Team section start =================-->
<section class="team-area area-padding" style="background-color:#f7f7f7">
    <div class="pl-5 pr-5">
        <div class="area-heading row">
            <div class="col-12 text-center">
                <h3>Promo Terbaru dari kami</h3>
            </div>
        </div>

        <style>
            .carousel-indicators .active {
                background-color: #4286f4;
            }

            .carousel-indicators li {
                background-color: #71a6fc;
            }

            .carousel-inner {
                padding-bottom: 20px
            }

            .carousel-control-prev-icon,
            .carousel-control-next-icon {
                height: 100px;
                width: 100px;
                outline: transparent;
                background-size: 100%, 100%;
                border-radius: 50%;
                background-image: none;
            }

            .carousel-control-next-icon:after {
                content: '>';
                font-size: 70px;
                color: #4fc1bb;
            }

            .carousel-control-prev-icon:after {
                content: '<';
                font-size: 70px;
                color: #4fc1bb;
            }
        </style>

        <div id="promoCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @php
                $index = 0;
                @endphp
                @for ($i = 0; $i<count($promos)/3; $i++) <li data-target="#promoCarousel"
                    data-slide-to="{{$i}}" {!!$i==0 ? "class='carousel-line active'" :"class='carousel-line'"!!}>
                </li>
                @endfor
            </ol>
            <div class=" carousel-inner">
                    @php
                    $index=0;
                    @endphp
                    @foreach ($promos as $promo)
                    @if ($index%3==0)
                    <div class="carousel-item {{$index==0 ? 'active': ''}}">
                        <div class="row">
                            <div class=""></div>
                            @endif
                            <div class="col-4 ">
                                <div class="card card-team">
                                    <a href="{{$promo->image}}">
                                        <img class="card-img rounded-0" src="{{$promo->image}}" alt="">
                                    </a>
                                    <div class="card-team__body text-center">
                                        <h3><a href="#">{{$promo->title}}</a></h3>
                                        <p>{{$promo->description}}</p>
                                        <div class="team-footer  justify-content-between">
                                            <p><h6>Berlaku pada tanngal</h6></p>
                                            <p>{{$promo->date_start." - ".$promo->date_end}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if ($index%3==2 || $index >= count($promos))
                        </div>
                    </div>
                    @endif
                    @php
                    $index++;
                    @endphp
                    @endforeach

            </div>
        </div>

        <a class="carousel-control-prev" href="#promoCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true" style="color:red"></span>
            <span class="sr-only" style="color:black">Previous</span>
        </a>
        <a class="carousel-control-next" href="#promoCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
<!--================ Team section end =================-->

<!--================End Home Banner Area =================-->