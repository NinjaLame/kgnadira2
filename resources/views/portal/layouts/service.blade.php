<!--================ Service section start =================-->

<div class="service-area area-padding-top">
    <div class="container">
        <div class="area-heading row">
            <div class="col-md-5 col-xl-4">
                <h3>Awesome<br>
                    Health Service</h3>
            </div>
            <div class="col-md-7 col-xl-8">
                <p>Land meat winged called subdue without very light in all years sea appear midst forth image him third
                    there set. Land meat winged called subdue without very light in all years sea appear</p>
            </div>
        </div>
        @php
        $index = 0;
        @endphp
        @foreach ($services as $item)
        @php
        echo $index%3==0 ? "<div class='row'>" : '';
            @endphp
            <div class="col-md-6 col-lg-4 mb-3">
                <div class="card-service text-center text-lg-left mb-4 mb-lg-0">
                    <span class="card-service__icon">
                        <i class="{{$item->icon}}"></i>
                    </span>
                    <h3 class="card-service__title">{{$item->name}}</h3>
                    <p class="card-service__subtitle">{{$item->description}}</p>
                </div>
            </div>
            @php
            if ($index%3==2 || $index==count($services)) {
            echo '</div>';
        }
        $index++;
        @endphp
        @endforeach
        <div class="row ml-5">
            <div class="col-7 offset-3"></div>
            @php
            try {
            echo $services->links();
            } catch (\Exception $e) {
            // Do something exceptional
            }
            @endphp
        </div>
    </div>

</div>
<!--================ Service section end =================-->