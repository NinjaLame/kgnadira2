<!--================Header Menu Area =================-->
<header class="header_area">
        <div class="top_menu row m0">
            <div class="container">
                <div class="float-right">
                    <!-- <a class="dn_btn" href="mailto:medical@example.com"><i class="ti-email"></i>medical@example.com</a> -->
                    <a href="/location" class="dn_btn"> <i class="ti-location-pin"></i>Lokasi Klinik</a>
                    <a href="/faq" class="dn_btn"> <i class="ti-comments"></i>FAQ</a>
                    <a href="/about" class="dn_btn"> <i class="ti-info"></i>Tentang kami</a>
                </div>
            </div>	
        </div>	
        <div class="main_menu">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <a class="navbar-brand logo_h" href="index.html"><img src="https://klinikgiginadira.com/wp-content/uploads/2018/09/LOG-NADIRA-PNG.png" style="max-width: 150px"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                        <ul class="nav navbar-nav menu_nav ml-auto">
                            <li class="nav-item"><a class="nav-link" href="/">Beranda</a></li> 
                            <li class="nav-item"><a class="nav-link" href="/service">Perawatan</a></li> 
                            <li class="nav-item"><a class="nav-link" href="/schedule">Jadwal Praktek</a></li>
                            <li class="nav-item"><a class="nav-link" href="/promo">Promo</a></li>
                            <li class="nav-item"><a class="nav-link" href="/gallery">Galeri</a></li>
                            {{-- <li class="nav-item"><a class="nav-link" href="#">Karir</a></li> --}}
                            <!-- <li class="nav-item submenu dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Blog</a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a class="nav-link" href="blog.html">Blog</a></li>
                                    <li class="nav-item"><a class="nav-link" href="single-blog.html">Blog Details</a></li>
                                    <li class="nav-item"><a class="nav-link" href="element.html">element</a></li>
                                </ul>
                            </li>  -->
                            {{-- <li class="nav-item"><a class="nav-link" href="contact.html">Hubungi Kami</a></li> --}}
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <!--================Header Menu Area =================-->