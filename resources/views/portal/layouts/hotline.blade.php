<!-- ================ Hotline Area Starts ================= -->
<section class="hotline-area text-center area-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Hubungi Kami</h2>
                <span>{{ $about->phone }}</span>
                <br>
                <span>{{ $about->email }}</span>
                </p>
            </div>
        </div>
    </div>
</section>
<!-- ================ Hotline Area End ================= -->