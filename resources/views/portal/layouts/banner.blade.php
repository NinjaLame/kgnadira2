<!--================Home Banner Area =================-->
<!-- 
    <section class="banner-area d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <h1>Making Health<br>
                        Care Better Together</h1>
                    <p>Also you dry creeping beast multiply fourth abundantly our itsel signs bring our. Won form living. Whose dry you seasons divide given gathering great in whose you'll greater let livein form beast sinthete
                        better together these place absolute right.</p>
                    <a href="" class="main_btn">Make an Appointment</a>
                    <a href="" class="main_btn_light">View Department</a>
                </div>
            </div>
        </div>
    </section> -->

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        @php
        $index = 0;
        @endphp
        @foreach ($sliders as $slider)
        <li data-target="#carouselExampleIndicators" data-slide-to="{{$index}}" {{$index==0 ? "class='active'":""}}>
        </li>
        @php
        $index++;
        @endphp
        @endforeach
    </ol>
    <div class="carousel-inner">
        @php
        $index=0;
        @endphp
        @foreach ($sliders as $slider)
        <div class="carousel-item {{$index==0 ? 'active': ''}}">
            <img class="d-block w-100" src="{{$slider->img}}" alt="{{$index}} slide">
            <div class="carousel-caption d-none d-md-block">
                <h1>{{$slider->title}}</h1>
                <p>{{$slider->text}}</p>
            </div>
            @php
            $index++;
            @endphp
        </div>
        @endforeach

    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<!--================End Home Banner Area =================-->