<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">Sign In</h5>
                    <form class="form-signin" action="/login" method="POST">
                        @csrf
                        <div class="form-label-group">
                                <input type="email" id="inputEmail" name="email"
                                class="form-control @error('email') is-invalid @enderror" placeholder="Email address"
                                required autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            
                            <label for="inputEmail">Email address</label>

                        </div>

                        <div class="form-label-group">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <input type="password" name="password" id="inputPassword"
                                class="form-control  @error('password') is-invalid @enderror" placeholder="Password"
                                required>
                            <label for="inputPassword">Password</label>

                        </div>
                        <button class="genric-btn info circle btn-block text-uppercase" type="submit">Sign in</button>
                        <hr class="my-4">
                        <button class="genric-btn success circle btn-block text-uppercase" type="submit">
                            Daftar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>