<section class="container pb-3">
    @foreach ($locations as $location)
    <div class="row pb-2">
        <div class="col-10 offset-1">
            <div class="card mt-3">
                <div class="card-header">
                    <i class="ti-location-pin"></i> {!! $location->name ?? "Klinik Gigi" !!} |
                    {{ $location->phone ?? "08123456XXX" }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-10">
                            <div class="row">
                                <div class="col-12">
                                    <h5>Alamat Klinik</h5>
                                    <p class="card-text">{{$location->address ?? "Klinik Gigi Nadira Semarang"}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <h5>Hari Buka</h5>
                                    <p class="card-text">{{$location->day_open ?? "Klinik Gigi Nadira Semarang"}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <h6>Jam Operasional</h6>
                                    <p class="card-text">{{$location->opened_at ?? '00.00'}} -
                                        {{$location->closed_at ?? '00.00'}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-2 tex-center">
                            <div class="row">
                                <div class="col-12">
                                    <a href="https://www.google.com/maps/search/{{$location->address ?? 'Klinik Gigi Nadira Semarang'}}"
                                        class="genric-btn success circle">Lihat Map</a>
                                </div>
                            </div>
                            <div class="row pt-4">
                                <div class="col-12 d-flex">
                                    <a href="/schedule/{{$location->id ?? '0'}}" class="genric-btn info circle">List Dokter</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach

    <div class="row pt-5">
        <div class="col-5 offset-2">
            @php
            try {
            echo $locations->links();
            } catch (\Throwable $th) {
            }
            @endphp
        </div>
    </div>


</section>