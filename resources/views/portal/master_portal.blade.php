<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <!-- Required meta tags -->
    <link rel="icon" href="https://klinikgiginadira.com/wp-content/uploads/2018/01/cropped-icon-nadira-transparan-32x32.png" sizes="32x32" />
    <link rel="icon" href="https://klinikgiginadira.com/wp-content/uploads/2018/01/cropped-icon-nadira-transparan-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="https://klinikgiginadira.com/wp-content/uploads/2018/01/cropped-icon-nadira-transparan-180x180.png" />
    <title>Medcare Medical</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/portal/css/bootstrap.css">
    <link rel="stylesheet" href="/portal/css/themify-icons.css">
    <link rel="stylesheet" href="/portal/css/glyphter.css">
    {{-- <link rel="stylesheet" href="/portal/css/flaticon.css"> --}}
    <link rel="stylesheet" href="/portal/vendors/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="/portal/vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="/portal/vendors/animate-css/animate.css">
    <!-- main css -->
    <link rel="stylesheet" href="/portal/css/style.css">
    <link rel="stylesheet" href="/portal/css/responsive.css">

</head>
<body>
    @include('portal.layouts.header')

    @yield('content')

    @include('portal.layouts.footer')


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" src="js/app.js"></script>
<script src="/portal/js/jquery-2.2.4.min.js"></script>
<script src="/portal/js/popper.js"></script>
<script src="/portal/js/bootstrap.min.js"></script>
<script src="/portal/js/stellar.js"></script>
<script src="/portal/vendors/owl-carousel/owl.carousel.min.js"></script>
<script src="/portal/js/jquery.ajaxchimp.min.js"></script>
<script src="/portal/js/waypoints.min.js"></script>
<script src="/portal/js/mail-script.js"></script>
<script src="/portal/js/contact.js"></script>
<script src="/portal/js/jquery.form.js"></script>
<script src="/portal/js/jquery.validate.min.js"></script>
<script src="/portal/js/mail-script.js"></script>
<script src="/portal/js/theme.js"></script>
</body>
</html>