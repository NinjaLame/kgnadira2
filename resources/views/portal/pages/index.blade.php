@extends('portal.master_portal')

@section('title', 'Klinik Gigi Nadira')

@section('content')

@include('portal.layouts.banner')

@include('portal.layouts.service')

@include('portal.layouts.about')

@include('portal.layouts.appointment')

@include('portal.layouts.promo')

@append