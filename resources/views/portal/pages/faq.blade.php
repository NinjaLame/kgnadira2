@extends('portal.master_portal')


@section('title', 'FAQ')


@section('content')
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="container">
            <div class="banner_content d-md-flex justify-content-between align-items-center">
                <div class="mb-3 mb-md-0">
                    <h2>FAQ</h2>
                    <p>Jawaban terbaik dari yang sering ditanyakan kepada kami!</p>
                </div>
                <div class="page_link">
                    <a href="/">Home</a>
                    <a href="#">FAQ</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ appointment Area Starts =================-->
<section class="appointment-area">
    <div class="container">
        <div class="appointment-inner">
            <div class="row">
                <div class="col-sm-12 col-lg-10 offset-lg-1">
                    <div class="row">
                        <div class="col-sm-12 col-lg-5">
                            <h3>Have Some Questions?</h3>
                        </div>
                        <div class="col-sm-12 col-lg-5 offset-lg-2 text-right">
                            <div id="app">
                                <search-faq></search-faq>
                            </div>
                        </div>
                    </div>
                    <div class="accordion" id="accordionExample">
                        <?php $index = 0; ?>
                        @foreach ($faqs as $faq)
                        <div class="card">
                            <div class="card-header" id="heading{{$faq->id}}">
                                <h5 class="mb-0">
                                    <button class="btn btn-link {{
                                            $index == 0 ? '' : 'collapsed'
                                        }}" type="button" data-toggle="collapse" data-target="#collapse{{$faq->id}}"
                                        aria-expanded="true" aria-controls="collapse{{$faq->id}}">
                                        {{$faq->question}}
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse{{$faq->id}}" class="collapse {{ $index == 0 ? 'show' : '' }}"
                                aria-labelledby="heading{{$faq->id}}" data-parent="#accordionExample">
                                <div class="card-body">
                                    {{$faq->answer}}
                                </div>
                            </div>
                        </div>
                        <?php $index++; ?>
                        @endforeach
                    </div>
                    @php
                    try {
                    echo $faqs->links();
                    } catch (\Exception $e) {
                    // Do something exceptional
                    }
                    @endphp
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ appointment Area End =================-->

@append