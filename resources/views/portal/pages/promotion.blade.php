@extends('portal.master_portal')


@section('title', 'Promo')


@section('content')

<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="container">
            <div class="banner_content d-md-flex justify-content-between align-items-center">
                <div class="mb-3 mb-md-0">
                    <h2>Promo</h2>
                    <p>Promo-promo menarik untuk anda!</p>
                </div>
                <div class="page_link">
                    <a href="/">Home</a>
                    <a href="#">Perawatan</a>
                </div>
            </div>
        </div>
    </div>
</section>

@include('portal.layouts.promo')

@append