@extends('portal.master_portal')


@section('title', 'Lokasi Klinik')


@section('content')

<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="container">
            <div class="banner_content d-md-flex justify-content-between align-items-center">
                <div class="mb-3 mb-md-0">
                    <h2>Lokasi Klinik</h2>
                    <p></p>
                </div>
                <div class="page_link">
                    <a href="/">Home</a>
                    <a href="#">Klinik</a>
                </div>
            </div>
        </div>
    </div>
</section>

@include('portal.layouts.location')

@append