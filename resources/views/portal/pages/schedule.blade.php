@extends('portal.master_portal')


@section('title', 'Jadwal Praktek')


@section('content')

<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="container">
            <div class="banner_content d-md-flex justify-content-between align-items-center">
                <div class="mb-3 mb-md-0">
                    <h2>Jadwal Praktek</h2>
                </div>
                <div class="page_link">
                    <a href="/">Home</a>
                    <a href="#">Jadwal</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="appointment-area whole-wrap">
    <div class="container">
        <div class="row">
            <div id="app">
                <div class="col-12">
                    <div class="mb-30 mt-5 title_color"><h3>Jadwal Praktek</h3> <search-schedule></search-schedule></div> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
            <div class="progress-table-wrap ml-5">
                <div class="progress-table">
                    @if (!count($schedules))
                        {!! '<h2 class="text-center">Tidak ditemukan Jadwal</h2>'  !!}
                    @else
                    <div class="table-head">
                        <div class="serial">#</div>
                        <div class="visit">Klinik</div>
                        <div class="visit">Dokter</div>
                        <div class="visit">Hari</div>
                        <div class="visit">Waktu Praktik</div>
                    </div>
                    @endif
                    @php
                        $index = 1;
                    @endphp

                    @foreach ($schedules as $schedule)
                    <div class="table-row">
                        <div class="serial">{{$index++}}</div>
                        <div class="visit"><a href="/location/{{ $schedule->clinic->id }}">{{ ucfirst($schedule->clinic->name) }}</a></div>
                        <div class="visit"> {{ $schedule->doctor->user->name }} </div>
                        <div class="visit"> {{ ucfirst($schedule->day) }} </div>
                        <div class="visit"> {{ $schedule->started_at }} - {{ $schedule->finished_at}} </div>
                    </div>
                    @endforeach
                </div>
            </div>
            {{$schedules->links() ?? ''}}
            </div>
        </div>
    </div>
</section>



@append