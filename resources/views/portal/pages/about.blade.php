@extends('portal.master_portal')


@section('title', "About Us")


@section('content')

<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="container">
            <div class="banner_content d-md-flex justify-content-between align-items-center">
                <div class="mb-3 mb-md-0">
                    <h2>Tentang Kami</h2>
                    <p></p>
                </div>
                <div class="page_link">
                    <a href="/">Home</a>
                    <a href="#">About</a>
                </div>
            </div>
        </div>
    </div>
</section>

@include('portal.layouts.about')

<section class="sample-text-area">
    <div class="container">
        <h3 class="text-heading title_color">Tentang Kami</h3>
        <p class="sample-text">
            {{$about->desc}}
        </p>
    </div>
</section>\

@include('portal.layouts.hotline')
@append