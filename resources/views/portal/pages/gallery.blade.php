@extends('portal.master_portal')


@section('title', 'Galeri')


@section('content')

<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="container">
            <div class="banner_content d-md-flex justify-content-between align-items-center">
                <div class="mb-3 mb-md-0">
                    <h2>Galeri</h2>
                    <p></p>
                </div>
                <div class="page_link">
                    <a href="/">Home</a>
                    <a href="#">Galeri</a>
                </div>
            </div>
        </div>
    </div>
</section>

@include('portal.layouts.galery')

@append