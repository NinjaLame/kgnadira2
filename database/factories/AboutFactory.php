<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use App\About;

$factory->define(About::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'text_1' => $faker->realText(100,2),
        'text_2' => $faker->realText(200,2),
        'desc' => $faker->realText(200,2),
        'phone' => $faker->phoneNumber,
        'email' => $faker->email
    ];
});
