<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Faq;
use Faker\Generator as Faker;

$factory->define(Faq::class, function (Faker $faker) {
    return [
        'question' => $faker->realText(40,2),
        'answer' => $faker->realText(100,2),
        'status' => $faker->randomElement(['show','hide'],2)
    ];
});
