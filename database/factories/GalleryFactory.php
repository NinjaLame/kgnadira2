<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use App\Galery;

$factory->define(Galery::class, function (Faker $faker) {
    return [
        'image' => '/portal/img/elements/g5.jpg',
        'title' => $faker->word,
        'status' => $faker->randomElement(['show', 'hide'])
    ];
});
