<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use App\Service;

$factory->define(Service::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'icon' => $faker->randomElement(Service::$iconEnum,2),
        'description' => $faker->realText(100,2)
    ];
});
