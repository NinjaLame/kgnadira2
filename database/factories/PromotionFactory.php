<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Promotion;
use Faker\Generator as Faker;

$factory->define(Promotion::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'description' => $faker->realText(200,2),
        'image' => '/img/promo_default.png',
        'date_start' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'date_end' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'status' => $faker->randomElement(['show', 'hide'], 2)
    ];
});
