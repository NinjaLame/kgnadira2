<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use App\Slider;

$factory->define(Slider::class, function (Faker $faker) {
    return [
        'img' => '/portal/img/banner/banner.jpg',
        'title' => $faker->name,
        'text' => $faker->realText(200,2)
    ];
});
