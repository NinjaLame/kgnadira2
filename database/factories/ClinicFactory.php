<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use App\Clinic;

$factory->define(Clinic::class, function (Faker $faker) {
    return [
        'name' => $faker->realText(30,2),
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'day_open' => $faker->randomElement(['senin', 'selasa', 'rabu', 'kamis', 'jumat'],2),
        'opened_at' => $faker->time('H:i','now'),
        'closed_at' => $faker->time('H:i','now')
    ];
});
