<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use App\Schedule;

$factory->define(Schedule::class, function (Faker $faker) {
    return [
        'day' => $faker->randomElement(['senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu', 'minggu']),
        'started_at' => $faker->time('H:i'),
        'finished_at' => $faker->time('H:i'),
        'clinic_id' => rand(2,40)
    ];
});
