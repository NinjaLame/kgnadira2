<?php

use Illuminate\Database\Seeder;
use App\Faq;
use App\Slider;
use App\Service;
use App\About;
use App\Clinic;
use App\User;
use App\Doctor;
use App\Schedule;
use App\Promotion;
use App\Galery;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Clinic::class,50)->create();
        factory(User::class,1)->create()->each(function ($user) {
            $user->email = "admin@admin.com";
            $user->role = "admin";
            $user->save();
        });
        factory(User::class,20)->create()->each(function ($user)
        {
            if ($user->role == 'doctor') {
                factory(Doctor::class,1)->create()->each(function ($doctor) use ($user){
                    $doctor->user()->associate($user);
                    $doctor->schedules()->saveMany(factory(Schedule::class,4)->make());
                    $doctor->save();
                });

            }
        });
        factory(Faq::class,50)->create();
        factory(Service::class,5)->create();
        factory(Slider::class,4)->create();
        factory(About::class,1)->create();
        factory(Service::class, 5)->create();
        factory(Promotion::class,10)->create();
        factory(Galery::class,20)->create();
    }
}
