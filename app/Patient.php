<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\Schedule;

class Patient extends Model
{
    protected $guarded = ['id'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'email', 'dob', 'phone_number'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'dob' => 'date:Y-m-d',
    ];

    
    public function appointments()
    {
        return $this->hasMany(Appointment::class, 'id', 'patient_id');
    }

    public function user() 
    {
        return $this->belongsTo(User::class);
    }
}
