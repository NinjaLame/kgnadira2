<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinic extends Model
{
    protected $fillable = [
        'name', 'address', 'phone', 'day_open', 'opened_at', 'closed_at'
    ];
}
