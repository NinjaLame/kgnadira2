<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Schedule;

class Doctor extends Model
{
    protected $fillable = [
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }
}
