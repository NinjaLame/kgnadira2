<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'profile_picture', 'gender', 'status'
    ];

    static $roleEnum = ['admin', 'doctor', 'patient'];

    static $genderEnum = ['male', 'female'];

    static $statusEnum = ['enabled', 'disabled'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        return $this->role == 'admin';
    }

    public function isEmployee()
    {
        return $this->role == 'admin';
    }

    public function isDoctor()
    {
        return $this->role == 'doctor';
    }

    public function isPatient()
    {
        return $this->role == 'patient';
    }

    protected static function boot()
    {
        parent::boot();

        
        static::created(function($user){
            if ($user->role == 'doctor') {
                $doctor = Doctor::firstOrCreate(['user_id' => $user->id]);
                $doctor->save();
            } elseif ($user->role == 'patient') {
                $patient = Patient::firstOrCreate([
                    'user_id' => $user->id, 
                    'full_name'=>$user->name, 
                    'email' => $user->email
                ]);  
                $patient->save();
            }
        });

        static::updated(function ($user){
            if ($user->role == 'doctor') {
                $doctor = Doctor::firstOrCreate([
                    'user_id' => $user->id
                    ]);
                $doctor->save();
            } elseif ($user->role == 'patient') {
                $patient = Patient::firstOrCreate([
                    'user_id' => $user->id, 
                    'full_name'=>$user->name, 
                    'email' => $user->email
                ]);  
                $patient->save();
            }
        });
    }
}
