<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $fillable = [
        'token', 'expired_at', 'patient_id',
    ];

    public function patient()
    {
        $this->belongsTo(Patient::class, 'patient_id');
    }
}
