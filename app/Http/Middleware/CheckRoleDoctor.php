<?php

namespace App\Http\Middleware;

use Closure;

class CheckRoleDoctor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            switch (Auth::user()->role) {
                case "admin":
                case "employee":
                case "doctor":
                case "patient":
                default:
                break;
            }
        }
        return $next($request);
    }
}
