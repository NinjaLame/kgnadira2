<?php

namespace App\Http\Middleware;

use Closure;

class CheckRolePatient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            if ($this->auth->user()->role == 'patient') {
                return $next($request);
            }
        }
        return abort(404);
    }
}
