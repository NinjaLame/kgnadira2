<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\About;

class AboutController extends Controller
{
    public function index()
    {
        $about = Cache::remember('about', now()->addHours(1), function () {
            return About::orderBy('created_at', 'desc')->first();           
        });
        return view('portal.pages.about', compact('about'));
    }
}
