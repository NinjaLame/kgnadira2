<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Galery;

class GalleryController extends Controller
{
    public function index()
    {
        $images = Galery::orderBy('created_at', 'desc')->paginate(9);
        return view('portal.pages.gallery', compact('images'));
    }
}
