<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Promotion;

class PromoController extends Controller
{
    public function index()
    {
        $promos = Promotion::orderBy('created_at','desc')->take(5)->get();
        return view('.portal.pages.promotion', compact('promos'));
    }
}
