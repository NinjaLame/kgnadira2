<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Service;
use Illuminate\Support\Facades\Input;

class ServiceController extends Controller
{
    public function index()
    {
        $currentPage = Input::has('page') ? (int)Input::get('page') : 1;

        $services = Cache::remember('services_page'.$currentPage, now()->addHours(1), function () {
            return Service::orderBy('created_at', 'desc')->paginate(9);
        });
        return view('portal.pages.service', compact('services'));
    }
}
