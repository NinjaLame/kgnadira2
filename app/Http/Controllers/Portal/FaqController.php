<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;

class FaqController extends Controller
{
    public function index()
    {
        $currentPage = Input::has('page') ? (int)Input::get('page') : 1;
        if (Input::has('search')) {
            $faqs = Faq::where('question','like',"%".Input::get('search')."%")->get();
        } else {
            $faqs = Cache::remember('faqs'.$currentPage, now()->addHour(1), function () {
                return Faq::orderBy('created_at', 'desc')->paginate(10);
            });
        }
        return view('portal.pages.faq', compact('faqs'));
    }
}
