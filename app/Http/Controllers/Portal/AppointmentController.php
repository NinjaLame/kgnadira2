<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Patient;
use App\Service;
use App\Appointment;

class AppointmentController extends Controller
{
    public function index()
    {
        $patient = Patient::all()->first();
        $service = Service::all()->first();
        $appointment = new Appointment();

        $appointment->patient()->associate($patient);
        $appointment->status = "pending";
        $service->appointments()->save($appointment);

        $appointment->save();
        return "heyo";
    }
}
