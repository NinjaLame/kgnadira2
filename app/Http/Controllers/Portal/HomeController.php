<?php

namespace App\Http\Controllers\Portal;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\SecretCodeGuestMail;
use App\Patient;
use App\Appointment;
use App\Faq;
use Illuminate\Support\Facades\Cache;
use App\Slider;
use App\About;
use App\Service;
use App\Promotion;

class HomeController extends Controller
{
    public function index()
    {
        $faqs = Cache::remember('faq', now()->addHours(1), function () {
            return Faq::orderBy('created_at', 'desc')->get()->take(5);
        });

        $sliders = Cache::remember('slider', now()->addHours(1), function () {
            return Slider::orderBy('created_at', 'desc')->get();
        });

        $about = Cache::remember('about', now()->addDay(1), function () {
            return About::all()->first();           
        });

        $services = Cache::remember('service', now()->addHours(1), function () {
            return Service::orderBy('created_at', 'desc')->get()->take(3);
        });

        $promos = Promotion::orderBy('created_at','desc')->take(5)->get();

        return view('portal.pages.index', compact('faqs', 'sliders', 'about', 'services', 'promos'));
    }
    
    public function appointment(Request $request)
    {
        $data = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'email|required',
            'message' => 'max:255'
        ]);

        $patient = Patient::where('email',$request->input('email'))->first();
        // dd($request->all());
        if (!$patient) {
            $patient = new Patient();
            $patient->full_name = $request->input('name');
            $patient->email = $request->input('email');
            $patient->save();
        }

        $appointment = new Appointment();
        $appointment->patient_id = $patient->id;
        $appointment->save();
        // Mail::to($request->input('email'))->send(new SecretCodeGuestMail());

        return redirect('/');

    }
}
