<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Schedule;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use App\Clinic;

class ScheduleController extends Controller
{
    public function index()
    {
        $curremtPage = Input::has('page') ? (int)Input::get('page') : 1;
        $search = Input::has('search') ? Input::get('search') : null;
        if ($search != null) {
            $curremtPage = $curremtPage.$search;
        }
        $schedules = Cache::remember('schedule_service'.$curremtPage, now()->addSecond(60), function () use($search) {
            $schedule = Schedule::join('clinics', 'clinic_id', '=', 'clinics.id')
            ->join('doctors', 'doctor_id', '=', 'doctors.id')
            ->join('users', 'doctors.user_id', '=', 'users.id')
            ->orderBy('clinics.name', 'asc')
            ->orderByRaw("schedules.day = 'senin' DESC")
            ->orderByRaw("schedules.day = 'selasa' DESC")
            ->orderByRaw("schedules.day = 'rabu' DESC")
            ->orderByRaw("schedules.day = 'kamis' DESC")
            ->orderByRaw("schedules.day = 'jumat' DESC")
            ->orderByRaw("schedules.day = 'sabtu' DESC")
            ->orderByRaw("schedules.day = 'minggu' DESC");
            

            if ($search != null) {
                $schedule->where('schedules.day', 'like', "%".$search."%")
                ->orWhere('users.name', 'like', "%".$search."%")
                ->orWhere('clinics.name', 'like', "%".$search."%")
                ->orWhere('schedules.day', 'like', "%".$search."%");
            }

            return $schedule->select('schedules.*')->with(['clinic', 'doctor' => function ($q) {
                $q->with('user')->get();
            }])->paginate(8);
        });
        
        return view('portal.pages.schedule', compact('schedules'));
    }

    public function search(Clinic $clinic)
    {
        $curremtPage = Input::has('page') ? (int)Input::get('page') : 1;
        $schedules = Cache::remember('schedule_service_clinic'.$curremtPage.$clinic->id, now()->addSecond(60), function () use($clinic) {
            return Schedule::join('clinics', 'clinic_id', '=', 'clinics.id')
                    ->join('doctors', 'doctor_id', '=', 'doctors.id')
                    ->orderBy('clinics.name', 'asc')
                    ->orderByRaw("schedules.day = 'senin' DESC")
                    ->orderByRaw("schedules.day = 'selasa' DESC")
                    ->orderByRaw("schedules.day = 'rabu' DESC")
                    ->orderByRaw("schedules.day = 'kamis' DESC")
                    ->orderByRaw("schedules.day = 'jumat' DESC")
                    ->orderByRaw("schedules.day = 'sabtu' DESC")
                    ->orderByRaw("schedules.day = 'minggu' DESC")
                    ->select('schedules.*')
                    ->where('schedules.clinic_id', '=', $clinic->id)
                    ->with(['clinic', 'doctor' => function ($q) {
                        $q->with('user')->get();
                    }])->paginate(8);
        });

        // dd($schedules);
        
        return view('portal.pages.schedule', compact('schedules'));
    }
}
