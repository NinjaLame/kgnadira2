<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Clinic;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;

class LocationController extends Controller
{
    public function index()
    {
        $curremtPage = Input::has('page') ? (int)Input::get('page') : 1;
        $locations = Cache::remember('locations'.$curremtPage, now()->addHours(1), function () {
            return Clinic::paginate(5);
        });
        return view('portal.pages.locations', compact('locations'));
    }

    public function search(Clinic $clinic)
    {
        $locations = [$clinic];

        return view('portal.pages.locations', compact('locations'));
    }
}
