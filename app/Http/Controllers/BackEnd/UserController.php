<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255'],
            'password' => ['string', 'nullable'],
            'address' => ['required', 'string', 'max:255'],
            'gender' => ['required', 'string', 'in:male,female'],
            'role' => ['required', 'string', 'max:255'],
            'status' => ['required', 'string', 'max:255'],
            'profile_picture' => ['nullable', 'image', 'mimes:jpg,jpeg,png', 'max:100000'],
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }
        return $validator->valid();
    }

    public function index()
    {
        return view('sistem.pages.user', [
            'users' => User::orderBy('created_at', 'desc')->paginate(10)
        ]);
    }

    public function store(Request $request)
    {
        $data = $this->validator($request->all());

        $user = new User();

        if ($request->hasfile('profile_picture')) {
            $file = $request->file('profile_picture');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time() . '.' . $extension;
            $file->move('uploads/user/', $filename);
            $user->profile_picture = '/uploads/user/' . $filename;

        } else {
            $user->profile_picture = '/img/no_image.png';
        }

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->address = $data['address'];
        $user->gender = $data['gender'];
        $user->role = $data['role'];
        $user->status = $data['status'];

        $user->save();

        return redirect()->route('admin.user.index');
    }

    public function update(User $user, Request $request)
    {
        $data = $this->validator($request->all());

        if($request->hasfile('profile_picture')) 
        { 
            $file = $request->file('profile_picture');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename =time().'.'.$extension;
            $file->move('uploads/user/', $filename);
            $user->profile_picture = '/uploads/user/'.$filename;
        }

        
        if($data['password'] != null || !$request->has('password')){
            $user->password = Hash::make($data['password']);
        }
        $user->name = $data['name'];
        $user->email = $data['email'];
        
        $user->address = $data['address'];
        $user->gender = $data['gender'];
        $user->role = $data['role'];
        $user->status = $data['status'];

        $user->save();

        return redirect()->route('admin.user.index');
    }

    public function show(User $user)
    {
        return view('sistem.pages.user_edit', [
            'users' => User::orderBy('created_at', 'desc')->paginate(10),
            'selected' => $user
        ]);
    }

    public function destroy(User $user)
    {
        $user->delete();
        return back();
    }
}
