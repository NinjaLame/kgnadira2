<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Schedule;
use App\Doctor;
use App\Clinic;
use Illuminate\Support\Facades\Validator;

class ScheduleController extends Controller
{

    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'clinic_id' => ['required', 'numeric', 'min:1'],
            'doctor_id' => ['required', 'numeric', 'min:1'],
            'day' => ['required', 'string', 'max:40'],
            'started_at' => ['required', 'string', 'max:40'],
            'finished_at' => ['required', 'string', 'max:40'],
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }
        return $validator->valid();
    }

    public function index()
    {
        $schedules = Schedule::orderBy('creaated_at', 'desc')
            ->with(['clinic', 'doctor' => function ($q) {
                $q->with('user')->get();
            }])->paginate(10);

        // dd($schedules)

        $doctors = Doctor::join('users', 'user_id', '=', 'users.id')
            ->orderBy('users.name', 'asc')->select('doctors.*', 'users.name')
            ->get();


        $clinics = Clinic::orderBy('name', 'asc')->get();

        return view('sistem.pages.schedule', [
            'schedules' => $schedules,
            'doctors' => $doctors,
            'clinics' => $clinics
        ]);
    }


    public function store(Request $request)
    {
        $data = $this->validator($request->all());
        $schedule = new Schedule();
        $schedule->clinic_id = $data['clinic_id'];
        $schedule->doctor_id = $data['doctor_id'];
        $schedule->day = $data['day'];
        $schedule->started_at = $data['started_at'];
        $schedule->finished_at = $data['finished_at'];

        $schedule->save();

        return back();
    }   

    public function show(Schedule $schedule)
    {
        $selected = $schedule;
        $schedules = Schedule::orderBy('creaated_at', 'desc')
            ->with(['clinic', 'doctor' => function ($q) {
                $q->with('user')->get();
            }])->paginate(10);

        // dd($schedules)

        $doctors = Doctor::join('users', 'user_id', '=', 'users.id')
            ->orderBy('users.name', 'asc')->select('doctors.*', 'users.name')
            ->get();


        $clinics = Clinic::orderBy('name', 'asc')->get();

        return view('sistem.pages.schedule_edit', [
            'schedules' => $schedules,
            'doctors' => $doctors,
            'clinics' => $clinics,
            'selected' => $schedule
        ]);
    }

    public function destroy(Schedule $schedule)
    {
        $schedule->delete();
        return back();
    }
}
