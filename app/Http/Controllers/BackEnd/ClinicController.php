<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Clinic;
use Illuminate\Support\Facades\Validator;

class ClinicController extends Controller
{

    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:40'],
            'day_open' => ['required', 'string', 'max:40'],
            'opened_at' => ['required', 'string', 'max:40'],
            'closed_at' => ['required', 'string', 'max:40'],
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }
        return $validator->valid();
    }

    public function index()
    {
        $clinics = Clinic::orderBy('created_at', 'desc')->paginate(15);
        return view('sistem.pages.clinic', compact('clinics'));
    }

    public function store(Request $request)
    {
        $data = $this->validator($request->all());
        $clinic = new Clinic();
        $clinic->name = $data['name'];
        $clinic->address = $data['address'];
        $clinic->phone = $data['phone'];
        $clinic->day_open = $data['day_open'];
        $clinic->opened_at = $data['opened_at'];
        $clinic->closed_at = $data['closed_at'];
        $clinic->save();

        return redirect()->route('admin.clinic.index');
    }

    public function show(Clinic $clinic)
    {
        $clinics = Clinic::orderBy('created_at', 'desc')->paginate(10);
        $selected = $clinic;
        
        return view('sistem.pages.clinic_edit', compact('clinics', 'selected'));
    }

    public function update(Clinic $clinic, Request $request)
    {
        $data = $this->validator($request->all());
        $clinic->name = $data['name'];
        $clinic->address = $data['address'];
        $clinic->phone = $data['phone'];
        $clinic->day_open = $data['day_open'];
        $clinic->opened_at = $data['opened_at'];
        $clinic->closed_at = $data['closed_at'];
        $clinic->save();

        return redirect()->route('admin.clinic.index');
    }

    public function destroy(Clinic $clinic)
    {
        $clinic->delete();
        return back();
    }
}
