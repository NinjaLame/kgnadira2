<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Galery;
use Illuminate\Support\Facades\Validator;

class GaleryController extends Controller
{
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'image' => ['image','mimes:jpg,jpeg,png', 'max:100000'],
            'status' => ['in:show,hide']
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }
        return $validator->valid();
    }

    public function index()
    {
        $galery = Galery::orderBy('created_at', 'desc')->paginate(10);
        return view('sistem.pages.galery', compact('galery'));
    }

    public function store(Request $request)
    {
        $data = $this->validator($request->all());

        $galery = new Galery();

        if($request->hasfile('image')) 
        { 
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time().'.'.$extension;
            $file->move('uploads/galery/', $filename);
            $galery->image = '/uploads/galery/'.$filename;

        } else {
            $galery->image = '/img/no-image.jpg';
        }
        $galery->title = $data['title'];
        $galery->status = $data['status'];
        $galery->save();

        return redirect()->route('admin.galery.index');

    }

    public function show(Galery $galery)
    {
        $selected = $galery;
        $galery = Galery::orderBy('created_at', 'desc')->paginate(10);
        return view('sistem.pages.galery_edit', compact('galery', 'selected'));
    }

    public function update(Galery $galery, Request $request)
    {
        $data = $this->validator($request->all());

        if($request->hasfile('image')) 
        { 
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename =time().'.'.$extension;
            $file->move('uploads/galery/', $filename);
            $galery->image = '/uploads/galery/'.$filename;
        }
        
        $galery->title = $data['title'];
        $galery->status = $data['status'];
        $galery->save();

        return redirect()->route('admin.galery.index');
    }
    public function destroy(Galery $galery)
    {
        $galery->delete();
        return back();
    }
}
