<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Faq;

class FaqController extends Controller
{
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'answer' => ['required', 'string', 'max:255'],
            'question' => ['required', 'string', 'max:255'],
            'status' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }
        
        return $validator->valid();
    }

    public function index()
    {
        $faqs = Faq::orderBy('created_at', 'desc')->paginate(10);
        return view('sistem.pages.faq', compact('faqs'));
    }

    public function store(Request $request)
    {
        $data = $this->validator($request->all());
        $faq = new Faq();
        $faq->answer = $data['answer'];
        $faq->question = $data['question'];
        $faq->status = $data['status'];
        $faq->save();

        return redirect()->route('admin.faq.index');

    }

    public function show(Faq $faq)
    {
        $faqs = Faq::orderBy('created_at', 'desc')->paginate(10);
        $selected = $faq;
        return view('sistem.pages.faq_edit', compact('faqs', 'selected'));
    }

    public function update(Faq $faq, Request $request)
    {
        $data = $this->validator($request->all());
        $faq->answer = $data['answer'];
        $faq->question = $data['question'];
        $faq->status = $data['status'];
        $faq->save();

        return redirect()->route('admin.faq.index');
    }

    public function destroy(Faq $faq)
    {
        $faq->delete();
        return back();
    }
}
