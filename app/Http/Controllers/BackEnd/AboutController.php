<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\About;
use Illuminate\Support\Facades\Validator;

class AboutController extends Controller
{

    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'text_1' => ['required', 'string', 'max:255'],
            'text_2' => ['required', 'string', 'max:255'],
            'desc' => ['required', 'string'],
            'phone' => ['required', 'string', 'max:40'],
            'email' => ['required', 'string', 'max:40'],
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }
        return $validator->valid();
    }

    public function index()
    {
       
        $about = About::orderBy('created_at','desc')->first();
        if(!$about) {
            $about = new About();
            $about->title = '';
            $about->text_1 = '';
            $about->text_2 = '';
            $about->desc = '';
            $about->phone = '';
            $about->email = '';

            $about->save();
        }

        return view('sistem.pages.about', compact('about'));
    }

    public function store(Request $request)
    {
        $data = $this->validator($request->all());
        $clinic = About::find($data['id']);
        $clinic->title = $data['title'];
        $clinic->text_1 = $data['text_1'];
        $clinic->text_2 = $data['text_2'];
        $clinic->desc = $data['desc'];
        $clinic->phone = $data['phone'];
        $clinic->email = $data['email'];
        $clinic->save();

        return redirect()->route('admin.about.index');
    }
}
