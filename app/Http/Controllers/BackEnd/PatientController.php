<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Patient;
use App\User;
use Illuminate\Support\Facades\Validator;

class PatientController extends Controller
{

    public function validator()
    {
        $validator = Validator::make($data, [
            'answer' => ['required', 'string', 'max:255'],
            'question' => ['required', 'string', 'max:255'],
            'status' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }
        
        return $validator->valid();
    }
    public function index()
    {
        return view('sistem.pages.patient');
    }

    public function store(Request $request)
    {
        return redirect()->route('admin.patient.index');
    }
}
