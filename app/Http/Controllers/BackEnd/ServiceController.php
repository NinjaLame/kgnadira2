<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'icon' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }
        return $validator->valid();
    }

    public function index()
    {
        $services = Service::orderBy('created_at', 'desc')->paginate(10);
        return view('sistem.pages.service', compact('services'));
    }

    public function store(Request $request)
    {
        $data = $this->validator($request->all());
        $faq = new Service();
        $faq->name = $data['name'];
        $faq->description = $data['description'];
        $faq->icon = $data['icon'];
        $faq->save();

        return redirect()->route('admin.service.index');

    }

    public function show(Service $service)
    {
        $services = Service::orderBy('created_at', 'desc')->paginate(10);
        $selected = $service;
        return view('sistem.pages.service_edit', compact('services', 'selected'));
    }

    public function update(Service $service, Request $request)
    {
        $data = $this->validator($request->all());
        $service->name = $data['name'];
        $service->description = $data['description'];
        $service->icon = $data['icon'];
        $service->save();

        return redirect()->route('admin.service.index');
    }

    public function destroy(Service $service)
    {
        $service->delete();
        return back();
    }
}
