<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Promotion;

class PromoController extends Controller
{
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'image' => ['image','mimes:jpg,jpeg,png', 'max:100000'],
            'date_start' => ['string', 'max:40'],
            'date_end' => ['required', 'string', 'max:40'],
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }
        return $validator->valid();
    }

    public function index()
    {
        $promotions = Promotion::orderBy('created_at', 'desc')->paginate(10);
        // dd($promotions);
        return view('sistem.pages.promo', compact('promotions'));
    }

    public function store(Request $request)
    {
        $data = $this->validator($request->all());

        $promotion = new Promotion();

        if($request->hasfile('image')) 
        { 
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename =time().'.'.$extension;
            $file->move('uploads/promos/', $filename);
            $promotion->image = '/uploads/promos/'.$filename;

        } else {
            $promotion->image = '/img/promo_default.png';
        }
        // dd($data);
        $promotion->title = $data['title'];
        $promotion->description = $data['description'];
        $promotion->date_start = $data['date_start'];
        $promotion->date_end = $data['date_end'];
        $promotion->save();

        return redirect()->route('admin.promo.index');

    }

    public function show(Promotion $promo)
    {
        $promotions = Promotion::orderBy('created_at', 'desc')->paginate(10);
        $selected = $promo;
        
        return view('sistem.pages.promo_edit', compact('promotions', 'selected'));
    }

    public function update(Promotion $promo, Request $request)
    {
        $data = $this->validator($request->all());

        if($request->hasfile('image')) 
        { 
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename =time().'.'.$extension;
            $file->move('uploads/promos/', $filename);
            $promo->image = '/uploads/promos/'.$filename;
        }
        
        $promo->title = $data['title'];
        $promo->description = $data['description'];
        $promo->date_start = $data['date_start'];
        $promo->date_end = $data['date_end'];
        $promo->save();

        return redirect()->route('admin.promo.index');
    }

    public function destroy(Promotion $promo)
    {
        $promo->delete();
        return back();
    }
}
