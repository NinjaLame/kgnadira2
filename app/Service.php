<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    static $iconEnum = [
        'flaticon-tooth', 
        'flaticon-check-list',
        'flaticon-loupe',
        'flaticon-hospital',
        'flaticon-pills',
        'flaticon-microscope',
        'flaticon-stethoscope',
        'flaticon-shield'
    ];
    
    protected $fillable = [
        'name', 'description', 'icon'
    ];

    public function appointments()
    {
        return $this->hasMany(Appointment::class, 'id', 'service_id');
    }
}
